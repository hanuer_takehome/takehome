# -$-coding:utf-8-*-

"""

"""
from pyspark.sql.session import SparkSession
from pyspark.sql.types import LongType, StringType, StructField, StructType, \
    BooleanType, ArrayType, IntegerType, TimestampType
import pymysql
from kafka import KafkaProducer


db_info = {
    'db_connection_url':'jdbc:mysql://172.28.0.4:3306/skt_db',
    'host':'172.28.0.4',
    'driver':'org.mariadb.jdbc.Driver',
    'user':'root',
    'password':'admin1234!',
    'cursorType':pymysql.cursors.DictCursor,
    'charSet':'utf8mb4'
}


class driver_passenger_schema:
    schema = StructType([ \
         StructField("id", IntegerType()), \
         StructField("date_created", StringType()), \
         StructField("name", StringType())])

class booking_schema:
    schema = StructType([ \
         StructField("id", IntegerType()), StructField("date_created", StringType()), \
         StructField("id_driver", IntegerType()), StructField("id_passenger", IntegerType()), \
         StructField("rating", IntegerType()), StructField("start_date", StringType()), \
         StructField("end_date", StringType()), StructField("tour_value", IntegerType())])


class Singleton(object):

    _instances = {}

    def __new__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__new__(cls, *args, **kwargs)
        return cls._instances[cls]



class SKTContext(Singleton):

    def __init__(self):
        self.spark = SparkSession.builder.getOrCreate()
        self.spark.sparkContext.setLogLevel('WARN')

    def ddl_create_db(self, db_name):

        try:
            connectionInstance = pymysql.connect(host=db_info['host'], \
                                                 user=db_info['user'], \
                                                 password=db_info['password'], \
                                                 charset=db_info['charSet'], \
                                                 cursorclass=db_info['cursorType'])

            cursorInsatnce = connectionInstance.cursor()
            sqlStatement = 'create database ' + db_name
            cursorInsatnce.execute(sqlStatement)

            sqlQuery = 'show databases;'
            cursorInsatnce.execute(sqlQuery)

            databaseList = cursorInsatnce.fetchall()
            for datatbase in databaseList:
                print(datatbase)

        except Exception as e:
            print("Exeception occured:{}".format(e))

        finally:
            connectionInstance.close()
            cursorInsatnce = connectionInstance.cursor()

    def ddl_create_table(self, db_name, create_table_ddl):

        try:
            connectionInstance = pymysql.connect(host=db_info['host'], \
                                                 user=db_info['user'], \
                                                 password=db_info['password'], \
                                                 charset=db_info['charSet'], \
                                                 cursorclass=db_info['cursorType'], \
                                                 db=db_name)

            cursorInsatnce = connectionInstance.cursor()
            sqlStatement = create_table_ddl
            cursorInsatnce.execute(sqlStatement)

            sqlQuery = 'show tables;'
            cursorInsatnce.execute(sqlQuery)
            databaseList = cursorInsatnce.fetchall()

            for datatbase in databaseList:
                print(datatbase)

        except Exception as e:
            print("Exeception occured:{}".format(e))

        finally:
            connectionInstance.close()
            cursorInsatnce = connectionInstance.cursor()

    # read csv file
    def read_csv(self, csv_path):

        return self.spark.read.load(csv_path, format="csv", sep=",", inferSchema="true", header="true", schema=driver_passenger_schema.schema)


    # read mariadb table
    def read_dbtable(self, query):

        table_query = "(%s) AS T" % query

        return self.spark.read.format('jdbc').options(url=db_info['db_connection_url'], \
                                             dbtable=table_query, \
                                             driver=db_info['driver'], \
                                             user=db_info['user'], \
                                             password=db_info['password']).load()


    # write mariadb table
    def add_dbtable(self, df, table_name):

        df.write.mode('append').format('jdbc').options(url=db_info['db_connection_url'], \
                                                   dbtable=table_name, \
                                                   driver=db_info['driver'], \
                                                   user=db_info['user'], \
                                                   password=db_info['password']).save()

    # write csv file to kafka topic
    def add_data_to_topic(self, file_path, topic):

        producer = KafkaProducer(bootstrap_servers='172.28.0.3:9092')

        with open(file_path, 'r') as reader:
            for line in reader:
                line = line[:len(line)-1]
                producer.send(topic, key=None, value=line.encode('utf-8'))
     
    # parse from kafka message
    def parse_data_from_kafka_message(self, sdf):

        from pyspark.sql.functions import split
        assert sdf.isStreaming == True, "DataFrame doesn't receive streaming data"
        col = split(sdf['value'], ',')
        for idx, field in enumerate(booking_schema.schema):
            sdf = sdf.withColumn(field.name, col.getItem(idx).cast(field.dataType))

        return sdf.select([field.name for field in booking_schema.schema])

    def print_task_title(self, task):

        print ('#############################################################################################')
        print ('########################################### {} #########################################'.format(task))
        print ('#############################################################################################')


