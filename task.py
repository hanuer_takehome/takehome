import skt_common
from kafka import KafkaProducer
import time

def send_to_kafka_task02(rows): 
    producer = KafkaProducer(bootstrap_servers = ['172.28.0.3:9092']) 
     
    for row in rows: 
        line = str(row['week_num']) + ',' + \
        str(row['rank']) + ',' + \
        str(row['id_driver']) + ',' + \
        str(row['driver_name']) + ',' + \
        str(row['avg_rating']) + ',' + \
        str(row['driving_cnt']) 

        producer.send('task02', key=None, value=line.encode('utf-8')) 
        producer.flush() 

# TASK03 send to kafka topic
def send_to_kafka_task03(rows): 
    producer = KafkaProducer(bootstrap_servers = ['172.28.0.3:9092']) 
    
    for row in rows: 
        line = str(row['rank']) + ',' + \
            str(row['id_driver']) + ',' + \
            str(row['id_passenger']) + ',' + \
            str(row['tour_count']) + ',' + \
            str(row['avg_rating']) 
        
        producer.send('task03', key=None, value=line.encode('utf-8')) 
        producer.flush()

# TASK04 send to kafka topic
def send_to_kafka_task04(rows): 
    producer = KafkaProducer(bootstrap_servers = ['172.28.0.3:9092']) 
    
    for row in rows: 
        line = str(row['report_year']) + ',' + \
            str(row['total_revenue']) + ',' + \
            str(row['total_booking']) + ',' + \
            str(row['avg_tour_value']) + ',' + \
            str(row['avg_driving']) + ',' + \
            str(row['avg_driving_minutes']) + ',' + \
            str(row['best_tour_value_driver_id']) + ',' + \
            str(row['max_tour_value']) + ',' + \
            str(row['worst_tour_value_driver_id']) + ',' + \
            str(row['min_tour_value']) + ',' + \
            str(row['most_used_passenger_id']) + ',' + \
            str(row['booking_used_number']) 

        producer.send('task04', key=None, value=line.encode('utf-8')) 
        producer.flush()

if __name__ == '__main__':

    ctx = skt_common.SKTContext()
    spark = ctx.spark
    
    # create readstream 
    dsraw = spark.readStream.format('kafka') \
        .option('kafka.bootstrap.servers', '172.28.0.3:9092') \
        .option('subscribe', 'booking') \
        .option('startingOffsets', 'earliest').load().selectExpr("CAST(value AS STRING)")

    # add schema
    df = ctx.parse_data_from_kafka_message(sdf=dsraw)
    
    # prepare query
    query = df.select("*")
    
    # for in memory task
    queryStream = query.writeStream.queryName('booking').outputMode("append").format("memory").start()

    # read data time
    time.sleep(20)
    
    # base table (booking)
    spark.sql("""

        select 
            cast(id as integer) as id, 
            cast(date_created as timestamp) as date_created, 
            cast(id_driver as integer) as id_driver, 
            cast(id_passenger as integer) as id_passenger, 
            cast(rating as integer) as rating, 
            cast(start_date as timestamp) as start_date, 
            cast(end_date as timestamp) as end_date, 
            cast(tour_value as integer) as tour_value 
        from 
            booking 
        where
            date_created between '2016-01-01 00:00:00:000' and '2016-12-31 23:59:59:999'

    """).registerTempTable('booking')

    # TASK 02
    spark.sql("""

        select
            id_driver,
            weekofyear(date_created) as week_num,
            count(id_driver) as driving_cnt,
            avg(rating) as avg_rating
        from
            booking
        group by
            id_driver,
            weekofyear(date_created)

    """).registerTempTable('week_avg_rating')

    query = """

        select * from driver

    """
    driver = ctx.read_dbtable(query=query)
    driver.registerTempTable('driver')

    query = """

        select * from passenger

    """
    driver = ctx.read_dbtable(query=query)
    driver.registerTempTable('passenger')
    

    task02 = spark.sql("""

        select
            week_top_10.week_num,
            week_top_10.rank,
            week_top_10.id_driver as id_driver,
            d.name as driver_name,
            round(week_top_10.avg_rating, 2) as avg_rating,
            week_top_10.driving_cnt
        from
        (
            select t.* from
            (
                select
                    *,
                    row_number() over (partition by week_num order by avg_rating desc, driving_cnt desc) as rank
                from
                    week_avg_rating
                order by
                    week_num,
                    avg_rating desc
            ) t
            where rank <= 10
        ) week_top_10 inner join driver d
        on week_top_10.id_driver = d.id
        order by
            week_top_10.week_num,
            week_top_10.rank

    """)
    ctx.print_task_title(task='TASK 02')
    task02.show(550, False)
    task02.repartition(1).foreachPartition(send_to_kafka_task02)
    #.toJSON().repartition(1).foreachPartition(send_to_kafka_task02)

    # TASK 03
    spark.sql("""

        select
            distinct
            id_driver,
            id_passenger,
            count(id_driver) over(partition by id_driver, id_passenger) as cnt
        from
            booking

    """).registerTempTable('count')

    spark.sql("""

        select
            b.id_driver,
            b.id_passenger,
            avg(b.rating) as avg_rating
        from booking b inner join count t
            on b.id_driver = t.id_driver
            and b.id_passenger = t.id_passenger
        group by
            b.id_driver,
            b.id_passenger

    """).registerTempTable('avg_rating')

    task03 = spark.sql("""

        select
            t.rank,
            t.id_driver,
            t.id_passenger,
            t.cnt as tour_count,
            round(t.avg_rating, 2) as avg_rating
        from
        (
            select
                row_number() over (order by c.cnt desc, r.avg_rating desc) as rank,
                c.id_driver,
                c.id_passenger,
                c.cnt,
                r.avg_rating
            from
                count c inner join avg_rating r
                on c.id_driver = r.id_driver
                and c.id_passenger = r.id_passenger

        ) t 
        where
            t.rank <= 10

    """)
    ctx.print_task_title(task='TASK 03')
    task03.show(20, False)
    task03.repartition(1).foreachPartition(send_to_kafka_task03)
    #toJSON().repartition(1).foreachPartition(send_to_kafka_task03)

    # TASK 04
    spark.sql("""

        select
            cast(id as integer) as id,
            date_format(cast(date_created as timestamp), 'yyyy') as date_created,
            cast(id_driver as integer) as id_driver,
            cast(id_passenger as integer) as id_passenger,
            cast(rating as integer) as rating,
            cast(start_date as timestamp) as start_date,
            cast(end_date as timestamp) as end_date,
            cast(tour_value as integer) as tour_value
        from 
            booking
        where
            date_created between '2016-01-01 00:00:00:000' and '2016-12-31 23:59:59:999'

    """).registerTempTable('booking')

    spark.sql("""
    
        select
            date_created,
            id_passenger,
            count(id_passenger) as cnt
        from
            booking
        group by
            date_created,
            id_passenger

    """).registerTempTable('booking_passenger')

    spark.sql("""
    
        select
            date_created,
            id_driver,
            sum(tour_value) as sum_tour_value,
            count(id) as cnt
        from
            booking
        group by
            date_created,
            id_driver
        
    """).registerTempTable('driver_tour_value')

    task04 = spark.sql("""
    
        select
            a.date_created as report_year,
            a.total_revenue,
            a.total_booking,
            round(a.avg_tour_value, 2) as avg_tour_value,
            b.avg_driving,
            round(c.avg_driving_minutes, 2) as avg_driving_minutes,
            d.id_driver as best_tour_value_driver_id,
            d.max_tour_value,
            e.id_driver as worst_tour_value_driver_id,
            e.min_tour_value,
            f.id_passenger as most_used_passenger_id,
            f.used_cnt as booking_used_number
        from
        -- total revenue
        -- total booking
        -- total avg_tour_value
        (
            select 
                date_created,
                sum(tour_value) as total_revenue,
                count(id) as total_booking,
                avg(tour_value) as avg_tour_value
            from 
                booking
            group by
                date_created
        ) a left outer join 

        -- average driving 
        (
            select
                date_created,
                avg(cnt) as avg_driving
            from
                driver_tour_value
            group by
                date_created
        ) b on a.date_created = b.date_created
    
        left outer join
        -- average driving time (minutes)
        (
            select
                date_created,
                (avg(avg_driving_time) / 60) as avg_driving_minutes
            from
            (
                select
                    date_created,
                    id,
                    unix_timestamp(end_date, 'HH:mm:ss') - unix_timestamp(start_date, 'HH:mm:ss') as avg_driving_time
                from 
                    booking
            ) t
            group by
                date_created
        ) c on b.date_created = c.date_created
    
        left outer join 
        -- most tour value, driver id
        (
            select
                date_created,
                id_driver,
                max(sum_tour_value) as max_tour_value
            from
                driver_tour_value
            group by
                id_driver,
                date_created
            order by
                max(sum_tour_value) desc
            limit 1
        ) d on c.date_created = d.date_created
    
        left outer join 
        -- worst tour value, driver id
        (
            select
                date_created,
                id_driver,
                min(sum_tour_value) as min_tour_value
            from
                driver_tour_value
            group by
                id_driver,
                date_created
            order by
                min(sum_tour_value)
            limit 1
        ) e on d.date_created = e.date_created
    
        left outer join 
        -- most used passenger id, used number
        (
            select
                date_created,
                id_passenger,
                max(cnt) as used_cnt
            from
                booking_passenger
            group by
                id_passenger,
                date_created
            order by
                max(cnt) desc
            limit 1
        ) f on e.date_created = f.date_created

    """)
    ctx.print_task_title(task='TASK 04')
    task04.show(5, False)
    task04.repartition(1).foreachPartition(send_to_kafka_task04)
    #toJSON().repartition(1).foreachPartition(send_to_kafka_task03)

    queryStream.awaitTermination()
