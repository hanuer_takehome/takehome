import skt_common

if __name__=='__main__':

    ctx = skt_common.SKTContext()
    spark = ctx.spark
    
    db_name = 'skt_db'
    ctx.ddl_create_db(db_name=db_name)

    # create driver table
    create_table_ddl = """

        CREATE TABLE `driver` (
            `id` BIGINT NOT NULL,
            `date_created` TIMESTAMP NOT NULL,
            `name` VARCHAR(255) NOT NULL,
            PRIMARY KEY(`id`)
        )
        ENGINE = InnoDB;

    """

    ctx.ddl_create_table(db_name=db_name, create_table_ddl=create_table_ddl) 
    
    # create passenger table 
    create_table_ddl = """ 

        CREATE TABLE `passenger` ( 
            `id` BIGINT NOT NULL, 
            `date_created` TIMESTAMP NOT NULL, 
            `name` VARCHAR(255) NOT NULL, 
            PRIMARY KEY(`id`)
        )
        ENGINE = InnoDB;

    """ 
        
    ctx.ddl_create_table(db_name=db_name, create_table_ddl=create_table_ddl) 
    
    # driver table  input 
    csv_path = '/app/csv_data/driver.csv' 
    driver_df = ctx.read_csv(csv_path=csv_path)
    ctx.add_dbtable(df=driver_df, table_name='driver') 
    
    # passenger table input 
    csv_path = '/app/csv_data/passenger.csv' 
    passenger_df = ctx.read_csv(csv_path=csv_path) 
    ctx.add_dbtable(df=passenger_df, table_name='passenger')

    # add booing.csv to kafka topic
    topic_name = 'booking' 
    csv_path = '/app/csv_data/booking.csv' 
    ctx.add_data_to_topic(topic=topic_name, file_path=csv_path)
