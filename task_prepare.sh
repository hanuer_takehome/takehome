#!/bin/bash

spark-submit \
	--num-executors 1 \
	--executor-memory 1g \
	--executor-cores 1 \
	--name skt_task \
	--jars /app/mariadb_client/mariadb-java-client-2.4.0.jar \
	--packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.3.1 \
	--py-files ./skt_common.py \
	--verbose ./task_prepare.py
